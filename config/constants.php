<?php

/**
* Constats
* 
* 
* @created    22/09/2018
* @package    Ak Clinics
* @copyright  Copyright (C) 2018
* @license    Proprietary
* @author     Mohit Thakur
*/

//Pagination Limit
define('PAGINATION_LIMIT', 10);