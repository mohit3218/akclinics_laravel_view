<?php
return [    
	[
        "title" => "Slugs",
        "icon_class" => "fa fa-user",
        "links" => [
            [
                "title" => "Summary",
                "route" => "admin/slugs",
                "other_routes" => [],
                "icon_class" => "fa fa-navicon",
				'roles' => array(1), //Role IDs
            ],
            [
                "title" => "Add Slugs",
                "route" => "admin/slugs/create",
                "other_routes" => [
					"admin/slugs/{slug}/edit"
				],
                "icon_class" => "fa fa-plus-circle",
				'roles' => array(1), //Role IDs
            ]
        ]
    ],
];