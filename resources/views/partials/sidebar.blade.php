<?php
$menus = config("menus");
$user = \Auth::user();
?>
<div class="page-sidebar navbar-collapse collapse">
    <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">                            
        @foreach($menus as $menu)
            <?php
                $active = false;
                $route = \Route::getCurrentRoute()->uri;
                foreach($menu['links'] as $link)
                {
                    if ($link["route"] == $route || in_array($route, $link["other_routes"]))
                    {
                        $active = true;
                    }
                }
            ?>
            <li class="nav-item start {{ $active ? "active open" : "" }}}">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="{{ $menu["icon_class"] }}"></i>
                    <span class="title">{{ $menu["title"] }}</span>
                    <span class="selected"></span>
                    <span class="arrow {{ $active ? "open" : ""}}"></span>
                </a>

                <ul class="sub-menu">
                    @foreach($menu['links'] as $link)
                    <?php $active = $link["route"] == $route || in_array($route, $link["other_routes"]); ?>
                            <li class="nav-item {{ $active ? "active open" : "" }}">
                                <a href="{{ url($link['route']) }}" class="nav-link">
                                    <i class="{{ $link['icon_class'] }}"></i>
                                    <span class="title">{{ $link['title'] }}</span>
                                    <span class="selected"></span>
                                </a>
                            </li>
                    @endforeach
                </ul>
            </li>
        @endforeach
    </ul>   
</div>

<script type="text/javascript">
    $("ul.sub-menu:not(:has(li))").parent().remove();
</script>