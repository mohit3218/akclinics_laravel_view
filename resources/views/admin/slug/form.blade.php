<?php
/**
 * Slug Add/Edit Form 
 * 
 * @created    10/01/2018
 * @package    AK Clinics
 * @copyright  Copyright (C) 2018
 * @author     Mohit Thakur
 */
?>
@extends('layouts.admin')
@section('content')

<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="">Home</a>
        </li>
        <li>
            <span>Slug</span>
        </li>
        <li>
            <span>Add</span>
        </li>
    </ul>
</div>
<div class="clearfix"></div>
@include('../partials/message')
<div class="form__structure">
    @if(isset($slug))
    {{ Form::model($slug, array('route' => array('slugs.update', $slug->id), 'class' => 'form-horizontal form-row-seperated', 'method' => 'PUT')) }}
    @else
    {!! Form::open(array('url' => 'admin/slugs', 'class' => 'form-horizontal form-row-seperated')) !!}
    @endif	
    {{ csrf_field() }}
    {{ Form::hidden('is_active', '1') }}
    <div class="form-body">
        <div class="form-group">
            {!! Form::label('Slug Name', 'Slug Name', array('class' => 'control-label col-md-4 col-sm-4 col-xs-12')) !!}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {!! Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'Slug Name', 'required' => 'required')) !!}
                <!--<input type="text" name="name" />-->
                @if ($errors->has('name'))
                <span class="help-block">
                    {{ $errors->first('name') }}
                </span>
                @endif
            </div>
        </div>
        <div class="action-buttons">
            <div class="col-md-8 col-sm-8 col-xs-12 col-md-offset-4 col-sm-offset-4 col-xs-offset-0">
                <button type="submit" class="btn blue">Submit</button>
                <!--{!! Form::submit('Submit', array('class' => 'btn blue')) !!}-->
                <button type="reset" class="btn grey">Cancel</button>
            </div> 
        </div>
    </div>
    {!! Form::close() !!}
</div>
@stop