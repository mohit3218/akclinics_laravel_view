<?php 
/**
 * Slug Index View - Shows Slug summary for Admin users
 * 
 * @created    10/01/2018
 * @package    AK Clinics
 * @copyright  Copyright (C) 2018
 * @author     Mohit Thakur
 */
?>
@extends('layouts.admin')
@section('content')
<!-- BEGIN PAGE BAR -->
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="index.html">Home</a>
		</li>
		<li>
			<span>Slug</span>
		</li>
		<li>
			<span>Summary</span>
		</li>
	</ul>
</div>
<!-- END PAGE BAR -->

<!-- END PAGE HEADER-->
<!-- BEGIN DASHBOARD STATS 1-->

<div class="clearfix"></div>
@include('../partials/message')
<!-- END DASHBOARD STATS 1-->
<div class="data__filter">
	<div class="form__structure shadow">
		<form class="form-horizontal form-row-seperated">
			<div class="row">
				<div class="col-md-12 col-xs-12">
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<div class="form-group">
								{!! Form::label('name', 'Slug Name', array('class' => 'control-label col-md-4 col-sm-4 col-xs-12')) !!}
								<div class="col-md-6 col-sm-6 col-xs-12">
									{!! Form::text('name', '', array('class' => 'form-control', 'placeholder' => 'Slug Name')) !!}
								</div>
							</div>
						</div>
					</div>
					<div class="action-buttons text-center">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<button type="submit" href="javascript:;" class="btn blue">Submit</button>
							<a href="{{ route('slugs.index') }}" class="btn default">Clear</a>
						</div> 
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<div class="table__structure">
	<div class="table__counters">
		Page {{ $records->currentPage() }} of {{ $records->lastPage() }} showing {{ $records->count() }} records out of {{ $records->total() }} total
	</div>
	<div class="table-responsive">
		<table class="table table-striped table-bordered table-checkable order-column" id="sample_1_2">
			<thead>
				<tr>
					<th> SR No. </th>
					<th> Slug Name </th>
					<th> Actions </th>
				</tr>
			</thead>
			<tbody>
				<?php $i = 1; ?>
				@foreach ($records as $record)
				<tr class="odd gradeX">
					<td> {{ $i++ }} </td>
					<td> {{ $record->name }} </td>
					<td>
						<a href="{{ route('slugs.edit', $record->id) }}" title="Edit" class="icon currency-color">
							<i class=" fa fa-edit"></i>
						</a>

						<a href="{{ route('slugs.destroy', $record->id) }}" title="Delete" class="icon lightBlue-color">
							<i class=" fa fa-trash"></i>
						</a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	<nav aria-label="Page navigation">
		<ul class="pagination">
			{{ $records->links() }}
		</ul>
	</nav>
</div>
@stop