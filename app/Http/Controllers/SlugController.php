<?php
/**
 * Slug Controller
 * 
 * @created    29/09/2018
 * @package    Ak Clinics
 * @copyright  Copyright (C) 2018
 * @license    Proprietary
 * @author     Mohit Thakur
 */

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;

use App\Slug;
use DB;

class SlugController extends AppController
{
	// Set for public Model name
	public $modelName = "Slug";
	
	//Define validation rules for the request
	public $rules = array(
        'name' => 'required'
    );
	
	/**
	* Display a listing of the resource.
	*
	* @return \Illuminate\Http\Response
	*/
    public function index()
    {
		$conditions = $this->getSearchConditions([
            ["view_field" => "name", "type" => "string"],
        ]);
		
		if($conditions)
		{
			//Paginate page according to search filter
			$records = Slug::sortable()->whereRaw($conditions)->paginate(PAGINATION_LIMIT);
		}
		else 
		{
			//Paginate page without search filter
			$records = Slug::sortable()->paginate(PAGINATION_LIMIT);
		}
        
        return view('admin.slug.index', ['records' => $records]);
	}
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Return Project Add form
		return view('admin.slug.form');
    }
	
	/**
	* Adds new record
	* @return type
	*/
    public function store($route = NULL)
    {
        //rote set for redirect the after save slugs summary
		$route = "admin/slugs";
		//Call parent method for add the Contact details
		return parent::add_record($route);
	}
    
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
		//Get a records in projects table
		$slug = Slug::findOrFail($id);
		//Set id and records to view
		return view('admin.slug.form', compact("id", "slug"));
    }
    
    /**
	* Update the specified resource in storage.
	* @param Slug $slug
	* @return type
	*/
    public function update(Slug $slug, $route = NULL)
    {
        //rote set for redirect the after save slugs summary
        $route = "admin/slugs";
		//Call parent method foe update the slug details
		return parent::edit_record($slug, $route);
	}
    
    /**
	* Deletes record
	* @param Contact $contact
	* @return type
	*/
    public function destroy(Slug $slug)
    {
		//Destroy function call the show method
    }
    
    /**
	* Show the form for editing the specified resource.
	*
	* @param  int  $id
	* @return \Illuminate\Http\Response
	*/
    public function show(Slug $slug)
    {
        //rote set for redirect the after save slugs summary
        $route = "admin/slugs";

        //Call a destroy action in main controller to delete a record soft delete
        return parent::delete_record($slug, $route);
    }
    
    /**
     * Status update Existing Record
     * @param type $id , $status
     */
    public function admin_toggleStatus(Slug $slug, $is_active) 
	{
        //rote set for redirect the after save slugs summary
        $route = "admin/slugs";

        //Call a destroy action in main controller to delete a record soft delete
        return parent::toggleStatus($slug, $is_active, $route);
    }
}
